﻿using Microsoft.EntityFrameworkCore;
using Otus.Clinic.Core.Domain;
using Otus.Clinic.DataAccess.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Clinic.DataAccess.Context
{
    public class EfContext : DbContext
    {
        public EfContext(DbContextOptions<EfContext> options) : base(options)
        {
            Database.EnsureDeleted();
            Database.EnsureCreated();
            this.WriteFake();
        }

        private void WriteFake()
        {
            this.Users.AddRange(FakeDataFactory.Users);

            this.SaveChanges();
        }

        public DbSet<User> Users { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
        }
    }
}
