﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Clinic.DataAccess.Context
{
    public static class EntityFrameworkConfigurator
    {
        public static IServiceCollection ConfigureContext(this IServiceCollection services, bool isDevelopment)
        {
            string connectionString = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile(isDevelopment ? "appsettings.Development.json" : "appsettings.json")
                .Build().GetConnectionString("DefaultConnection");

            DbContextOptionsBuilder<EfContext> optionsBuilder = 
                new DbContextOptionsBuilder<EfContext>();

            DbContextOptions<EfContext> options = 
                optionsBuilder.UseSqlServer(connectionString)
                //.UseSqlite(connectionString)
                .Options;

            services.AddSingleton(typeof(DbContext), new EfContext(options));

            return services;
        }
    }
}
