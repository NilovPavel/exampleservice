﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using Otus.Clinic.Core.Domain;

namespace Otus.Clinic.DataAccess.Data
{
    public static class FakeDataFactory
    {
        public static IEnumerable<User> Users
        {
            get
            {
                List<User> customerPreferences = new List<User>()
                {
                    new User
                    {
                        Id = Guid.NewGuid(),
                        FirstName = "Ivan",
                        LastName = "Ivanov",
                        MiddleName = "Ivanovich",
                    }
                };
                return customerPreferences;
            }
        }
    }
}