﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using Otus.Clinic.Core.Abstractions.Repositories;
using Otus.Clinic.Core.Domain;
using Otus.Clinic.DataAccess.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Clinic.DataAccess.Repositories
{
    public class EfRepository<T> : IRepository<T>
        where T : BaseEntity
    {
        private readonly EfContext _dataContext;
        public EfRepository(DbContext dataContext) 
        {
            _dataContext = dataContext as EfContext;

            if (_dataContext == null) 
                throw new Exception("Используемый в репозитории контекст данных не является допустимым.");
        }

        public Task<Guid> AddUser(User user)
        {
            User addedUser = _dataContext.Users.Add(user).Entity;
            if (addedUser == null) 
                return Task.FromResult(Guid.Empty);
            _dataContext.SaveChanges();
            return Task.FromResult(addedUser.Id);
        }

        public Task<T> EditUser(Guid id, User user)
        {
            User editedUser = _dataContext.Users.FirstOrDefault(item => item.Id.Equals(id));

            if (editedUser == null)
                return Task.FromResult(editedUser as T);

            editedUser.FirstName = user.FirstName;
            editedUser.LastName = user.LastName;
            editedUser.MiddleName = user.MiddleName;
            _dataContext.SaveChanges();

            return Task.FromResult(editedUser as T);
        }

        public Task<IEnumerable<T>> GetAllAsync()
        {
            return Task.FromResult(_dataContext.Users.ToList() as IEnumerable<T>);
        }

        public Task<T> GetByIdAsync(Guid id)
        {
            return Task.FromResult(_dataContext.Users.FirstOrDefault(item => item.Id.Equals(id)) as T);
        }

        Task<bool> IRepository<T>.DeleteUser(Guid id)
        {
            User editedUser = _dataContext.Users.FirstOrDefault(item => item.Id.Equals(id));

            if (editedUser == null)
                return Task.FromResult(false);

            _dataContext.Remove(editedUser);
            _dataContext.SaveChanges();

            return Task.FromResult(true);
        }
    }
}
