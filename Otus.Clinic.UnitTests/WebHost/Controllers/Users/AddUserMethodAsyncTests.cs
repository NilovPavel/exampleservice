﻿using AutoFixture;
using AutoFixture.AutoMoq;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Otus.Clinic.Core.Abstractions.Repositories;
using Otus.Clinic.Core.Domain;
using Otus.Clinic.WebHost.Controllers;
using Otus.Clinic.WebHost.Models;
using System;
using Xunit;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Users;

public class AddUserMethodAsyncTests
{
    private IFixture fixture;
    private readonly Mock<IRepository<User>> _userRepositoryMock;
    private readonly UserController _userController;

    public AddUserMethodAsyncTests()
    {
        this.fixture = new Fixture().Customize(new AutoMoqCustomization());

        this._userRepositoryMock = fixture.Freeze<Mock<IRepository<User>>>();
        this._userController = new UserController(this._userRepositoryMock.Object);
    }

    //Smock-test на создание пользователя
    [Fact]
    public async void AddUserMethodAsyncTests_UserAddToDB_ReturnOk()
    {
        _userRepositoryMock.Setup(repo => repo
                .AddUser(It.IsAny<User>())).ReturnsAsync(Guid.NewGuid());

        CreateOrEditUserRequest userRequest = new CreateOrEditUserRequest { };
        
        // Act
        var result = await _userController.CreateUserAsync(userRequest);

        // Assert
        result.Should().BeOfType<OkObjectResult>();

    }

    //Smock-test на создание пользователя
    [Fact]
    public async void AddUserMethodAsyncTests_UserAddToDB_ReturnNotFound()
    {
        _userRepositoryMock.Setup(repo => repo.AddUser(It.IsAny<User>())).ReturnsAsync(Guid.Empty);

        CreateOrEditUserRequest userRequest = new CreateOrEditUserRequest{ };

        // Act
        var result = await _userController.CreateUserAsync(userRequest);

        // Assert
        result.Should().BeOfType<NotFoundResult>();
    }

    [Fact]
    public async void AddUserMethodAsyncTests_RequestIsNull_ReturnBadRequest()
    {
        // Act
        var result = await _userController.CreateUserAsync(null);

        // Assert
        result.Should().BeOfType<BadRequestResult>();
    }
}