﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Otus.Clinic.Core.Domain;

namespace Otus.Clinic.Core.Abstractions.Repositories
{
    public interface IRepository<T>
        where T: BaseEntity
    {
        Task<IEnumerable<T>> GetAllAsync();
        
        Task<T> GetByIdAsync(Guid id);
        
        Task<Guid> AddUser(User user);
        
        Task<T> EditUser(Guid id, User user);

        Task<bool> DeleteUser(Guid id);
    }
}