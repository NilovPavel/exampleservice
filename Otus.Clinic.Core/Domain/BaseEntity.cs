﻿using System;

namespace Otus.Clinic.Core.Domain
{
    public class BaseEntity
    {
        public Guid Id { get; set; }
    }
}