﻿using Microsoft.AspNetCore.Mvc;
using Otus.Clinic.Core.Abstractions.Repositories;
using Otus.Clinic.Core.Domain;
using Otus.Clinic.WebHost.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Clinic.WebHost.Controllers
{
    /// <summary>
    /// Клиенты
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class UserController
        : ControllerBase
    {
        private readonly IRepository<User> _repository;
        public UserController(IRepository<User> repository)
        { 
            _repository = repository;
        } 

        /// <summary>
        /// Получение списка всех пользователей
        /// </summary>
        /// <returns>Возвращает список пользователей</returns>
        [HttpGet]
        public async Task<ActionResult<UserResponse>> GetCustomersAsync()
        {
            IEnumerable<User> users = await this._repository.GetAllAsync() as IEnumerable<User>;

            if (users == null)
                return NotFound();

            UserResponse response = new UserResponse
            { Users = users };

            return Ok(response);
        }
        
        /// <summary>
        /// Получение пользователя по идентификатору
        /// </summary>
        /// <param name="id">Идентификатор пользователя</param>
        /// <returns>Возвращает объект ответа пользователя</returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<UserResponse>> GetCustomerAsync(Guid id)
        {
            var customer = await _repository.GetByIdAsync(id);

            if (customer == null)
                return NotFound();

            var response = new UserResponse
            {
                Users = new List<User> { customer }
            };

            return Ok(response);
        }

        /// <summary>
        /// Создает объект пользователя и записывает его в БД
        /// </summary>
        /// <param name="request">Запрос, содержащий данные для заполнения</param>
        /// <returns>Возвращает Guid вновь созданного пользователя</returns>
        [HttpPost]
        public async Task<IActionResult> CreateUserAsync(CreateOrEditUserRequest request)
        {
            if (request == null) 
                return BadRequest();

            var user = new User
            {
                FirstName = request.FirstName,
                LastName = request.LastName,
                MiddleName = request.MiddleName
            };
            
            Guid guid = await _repository.AddUser(user);

            if (guid == Guid.Empty) 
                return NotFound();
            
            return Ok(guid);
        }
        
        /// <summary>
        /// Редактирует пользователя по Id
        /// </summary>
        /// <param name="id">Guid пользователя</param>
        /// <param name="request">Запрос на редактирование пользователя</param>
        /// <returns>Возвращает обновленную запись о пользователе</returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> EditCustomersAsync(Guid id, CreateOrEditUserRequest request)
        {
            User user = new User
            {
                FirstName = request.FirstName,
                LastName = request.LastName,
                MiddleName = request.MiddleName
            };

            var editUser = await _repository.EditUser(id, user);
            
            if (editUser == null)
                return NotFound();

            return Ok(editUser);
        }

        /// <summary>
        /// Удаление пользователя
        /// </summary>
        /// <param name="id">Guid пользователя</param>
        /// <returns>Возвращает ответ со строкой и Guid удаленного пользователя или ответ о неудаче</returns>

        [HttpDelete]
        public async Task<IActionResult> DeleteCustomer(Guid id)
        {
            bool isSuccess = await _repository.DeleteUser(id);

            if (!isSuccess)
                return NotFound(id);

            return Ok("Пользователь с id = " + id + " удален.");
        }
    }
}