﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Otus.Clinic.WebHost.Middleware;

namespace Otus.Clinic.Middleware;
public static class Extensions
{
    public static IApplicationBuilder UseLogging(this IApplicationBuilder builder)
    {
        if(builder == null)
            throw new ArgumentNullException(nameof(builder));

        return builder.UseMiddleware<LoggingMiddleware>();
    }

    public static IApplicationBuilder UseExceptionHandlingMiddleware(this IApplicationBuilder builder)
    {
        if (builder == null)
            throw new ArgumentNullException(nameof(builder));

        return builder.UseMiddleware<ExceptionHandlingMiddleware>();
    }
}
