﻿using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;

namespace Otus.Clinic.Middleware;

public class LoggingMiddleware
{
    private readonly RequestDelegate next;
    public LoggingMiddleware(RequestDelegate next)
    {
        this.next = next;
    }

    public async Task InvokeAsync(HttpContext httpContext, ILogger<LoggingMiddleware> logger)
    {
        var timer = new Stopwatch();
        timer.Start();
        await next.Invoke(httpContext);
        timer.Stop();
        var elapsedMs = timer.Elapsed;

        logger.Log(LogLevel.Information, httpContext.Request.Method,
            httpContext.Request.Query, httpContext.Request.QueryString,
            httpContext.Response?.StatusCode, elapsedMs.TotalMicroseconds);
    }
}