﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Otus.Clinic.Middleware;
using System;
using System.Diagnostics;
using System.Net;
using System.Threading.Tasks;

namespace Otus.Clinic.WebHost.Middleware;

public class ExceptionHandlingMiddleware
{
    private readonly RequestDelegate next;
    public ExceptionHandlingMiddleware(RequestDelegate next)
    {
        this.next = next;
    }

    public async Task InvokeAsync(HttpContext httpContext, 
        ILogger<ExceptionHandlingMiddleware> logger,
        IWebHostEnvironment webHostEnvironment)
    {
        httpContext.Request.EnableBuffering();
        try
        {
            await next(httpContext);
        }
        catch (Exception ex)
        {
            await HandleExceptionAsync(httpContext, ex, logger, webHostEnvironment);
        }
    }

    private Task HandleExceptionAsync(HttpContext httpContext, Exception ex, ILogger<ExceptionHandlingMiddleware> logger, IWebHostEnvironment webHostEnvironment)
    {
        logger.LogError(ex, ex.Message);
        var errorMessageDetails = string.Empty;

        if (!webHostEnvironment.IsProduction())
            errorMessageDetails = ex.Message;
        else
            errorMessageDetails = "Ошибка. Пожалуйста, обратитесь к администратору!";

        var result = JsonConvert.SerializeObject(new { error = errorMessageDetails });
        httpContext.Response.ContentType = "application/json";
        httpContext.Response.StatusCode = (int)HttpStatusCode.BadRequest;
        return httpContext.Response.WriteAsync(result);
    }
}
