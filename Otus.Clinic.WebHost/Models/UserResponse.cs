﻿using System.Collections.Generic;
using Otus.Clinic.Core.Domain;

namespace Otus.Clinic.WebHost.Models;

public class UserResponse
{
    public IEnumerable<User> Users { get; set; }
}
