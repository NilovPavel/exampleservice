﻿using System;

namespace Otus.Clinic.WebHost.Models;

public class CreateOrEditUserRequest
{
    public string LastName { get; set; }
    public string FirstName { get; set; }
    public string MiddleName { get; set; }
}
